#include "Pelicula.h"

int numeracion = 0;

Pelicula::Pelicula()
{
	this->codigo = 0;
	this->titulo = "";
	this->director = "";
	this->precioBase = 0;
}

Pelicula::Pelicula(int _Codigo, string _Titulo, string _Director, float _PrecioBase, TipoPelicula _TipoPelicula, int dia, int mes, int anio) : fechaEstreno(dia,mes,anio)
{
	this->codigo = _Codigo;
	this->titulo = _Titulo;
	this->director = _Director;
	this->precioBase = _PrecioBase;
	this->tipoPelicula = _TipoPelicula;

}

int Pelicula::GetCodigo()
{
	return this->codigo;
}

string Pelicula::GetTitulo()
{
	return this->titulo;
}

string Pelicula::GetDirector()
{
	return this->director;
}

float Pelicula::GetPrecio()
{
	return this->precioBase;
}

TipoPelicula Pelicula::GetTipoPelicula()
{
	return this->tipoPelicula;
}

Fecha Pelicula::GetFechaEstreno()
{
	return this->fechaEstreno;
}

bool Pelicula::EsEstreno()
{
	Fecha Hoy;
	if ((Hoy.RetornaAnio() - fechaEstreno.RetornaAnio()) < 5 )
	{
		return true;
	}
	else {
		return false;
	}
}

void Pelicula::ListarInformacion()
{
	cout << "Codigo: " << this->codigo << endl;
	cout << "Titulo: " << this->titulo << endl;
	cout << "Director: " << this->director << endl;
	cout << "Precio base: " << this->precioBase << endl;
	cout << "Tipo de Pelicula: " << this->tipoPelicula << endl;
}

float Pelicula::CalcularCosto()
{
	float costo;

	costo = this->GetPrecio();

	if (this->GetTipoPelicula() == Internacional)
	{
		costo = costo * 1.30;
	}
	else {
		if (this->EsEstreno() == false)
		{
			costo = costo * 0.8;
		}
	}
	return costo;
}

Pelicula::~Pelicula()
{
}
