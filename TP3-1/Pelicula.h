#pragma once
#include <iostream>
#include "Fecha.h"
using namespace std;

enum  TipoPelicula {Nacional, Internacional};

class Pelicula
{
	int codigo;
	string titulo;
	string director;
	float precioBase;
	TipoPelicula tipoPelicula;
	Fecha fechaEstreno;


public:
	//constructores
	Pelicula();
	Pelicula(int _Codigo, string _Titulo, string _Director, float _PrecioBase, TipoPelicula _TipoPelicula, int dia, int mes, int anio);

	//getters
	int GetCodigo();
	string GetTitulo();
	string GetDirector();
	float GetPrecio();
	TipoPelicula GetTipoPelicula();
	Fecha GetFechaEstreno();

	//metodos
	bool EsEstreno();
	void ListarInformacion();
	float CalcularCosto();

	//destructor
	~Pelicula();
};