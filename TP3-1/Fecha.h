#pragma once
namespace std {

	class Fecha {
		short int dia;
		short int mes;
		short int anio;
		bool esDiaValido() const;
		bool esMesValido() const;
		bool esAnioValido() const;
		short diasEnMes(const short int mes) const;
	public:
		Fecha();
		Fecha(short int dia, short int mes, short int anio);
		int RetornaDia();
		int RetornaMes();
		int RetornaAnio();
		void setFechaActual(void);
		~Fecha();
	};
}
