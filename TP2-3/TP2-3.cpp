#include <iostream>
#include "Vector.h"
int main()
{
	Vector V1;
	Vector V2(15, 0);
	Vector V3(V2);

	V1.MostrarVector();
	cout << endl;
	V2.MostrarVector();
	cout << endl;
	V3.MostrarVector();
	cout << endl;

	V2.InsertarElemento(5, 56);
	cout << V2.GetElemento(5)<<endl;
	V2.InsertarElemento(45, 56);
	V2.MostrarVector();
	cout << endl;

}

