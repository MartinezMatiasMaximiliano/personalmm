#pragma once
#include <iostream>

using namespace std;

class Vector
{
	int dim;
	int* arre = NULL;
public:
	// constructores
	Vector();
	Vector(int _Dim, int _Contenido);
	Vector(const Vector& _V);


	//getters
	int GetDimension();

	//metodos
	int GetElemento(int _Posicion);
	void BorrarElemento(int _Posicion);
	bool InsertarElemento(int _Posicion, int _Elemento);
	void Redimensionar(int _Dimension);

	void MostrarVector();

	//destructor
	~Vector();
};

