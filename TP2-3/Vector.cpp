#include "Vector.h"
#define indefinido 999
Vector::Vector()
{
	this->dim = 10;
	this->arre = new int[dim];
	for (int i = 0; i < dim; i++)
	{
		arre[i] = indefinido;
	}
}

Vector::Vector(int _Dim, int _Contenido)
{
	this->dim = _Dim;
	this->arre = new int[dim];
	for (int i = 0; i < dim; i++)
	{
		arre[i] = _Contenido;
	}
}

Vector::Vector(const Vector& _V)
{
	this->dim = _V.dim;
	this->arre = new int[dim];
	for (int i = 0; i < dim; i++)
	{
		arre[i] = _V.arre[i];
	}
}

int Vector::GetDimension()
{
	return this->dim;
}

int Vector::GetElemento(int _Posicion)
{
	
	return this->arre[_Posicion];
}

void Vector::BorrarElemento(int _Posicion)
{
	this->arre[_Posicion] = indefinido;
}

bool Vector::InsertarElemento(int _Posicion, int _Elemento)
{
	if (_Posicion < this->GetDimension())
	{
		this->arre[_Posicion] = _Elemento;
		cout << "Agregado con exito" << endl;
		return true;
	}
	else
	{
		cout << "Error: la posicion solicitada excede la capacidad del arreglo" << endl;
		return false;
	}
}

void Vector::Redimensionar(int _Dimension)
{
	
}

void Vector::MostrarVector()
{
	for (int i = 0; i < this->GetDimension(); i++)
	{
		cout << "[" << this->arre[i] << "]";
	}
}

Vector::~Vector()
{
	delete[] arre;
}

