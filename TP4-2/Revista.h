#pragma once
#include <iostream>
#include "Publicacion.h"
using namespace std;

enum Formato{Digital,Impreso};

class Revista : public Publicacion {
	string ISSN;
	Formato formato;

public:
	//constructores
	Revista();
	Revista(string _ISSN, string _Titulo, string _AnioEdicion, string _Autor, Formato _Formato);

	//getters
	string GetISSN();
	Formato GetFormato();

	//setters
	void SetISSN(string _ISSN);

	//metodos
	void MostrarInfo();//definir


	//destructores
	~Revista();

};