#include "Revista.h"

Revista::Revista()
{
}

Revista::Revista(string _ISSN, string _Titulo, string _AnioEdicion, string _Autor, Formato _Formato): Publicacion(_Titulo, _AnioEdicion, _Autor)
{
	this->ISSN = _ISSN;
	this->formato = _Formato;
	this->SetTipoPublicacion(eRevista);
}

string Revista::GetISSN()
{
	return this->ISSN;
}

Formato Revista::GetFormato()
{
	return this->formato;
}

void Revista::SetISSN(string _ISSN)
{
	this->ISSN = _ISSN;
}

void Revista::MostrarInfo()
{
	cout << "--------------------------------------------" << endl;
	cout << "[" << this->GetTitulo() << "]" << endl;
	cout << "Anio de Edicion: " << this->GetAnioEdicion() << endl;
	cout << "Autor: " << this->GetAutor() << endl;
	string aux;

	switch (this->GetTipoPublicacion())
	{
	case eLibro: aux = "Libro";		break;
	case eRevista: aux = "Revista";	break;
	case eTesis: aux = "Tesis";		break;
	default:break;
	}
	cout << "Tipo de publicacion: " << aux << endl;



	switch (this->GetFormato())
	{
	case Digital: aux = "Digital";break;
	case Impreso: aux = "Impreso";break;
	default:break;
	}
	cout << "Formato: " << aux << endl;



	cout << "ISSN: " << this->GetISSN() << endl;
	cout << "--------------------------------------------" << endl;
}

Revista::~Revista()
{
}
