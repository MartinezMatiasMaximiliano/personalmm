#include "Libro.h"

Libro::Libro()
{

}

Libro::Libro(string _ISBN, string _Titulo, string _AnioEdicion, string _Autor) : Publicacion(_Titulo, _AnioEdicion, _Autor)
{
	this->ISBN = _ISBN;
	this->SetTipoPublicacion(eLibro);
}

inline string Libro::GetISBN()
{
	return this->ISBN;
}

inline void Libro::SetISBN(string _ISBN)
{
	this->ISBN = _ISBN;
}


void Libro::MostrarInfo()
{
	cout << "--------------------------------------------" << endl;
	cout << "[" << this->GetTitulo() << "]" << endl;
	cout << "Anio de Edicion: " << this->GetAnioEdicion() << endl;
	cout << "Autor: " << this->GetAutor() << endl;

	string aux;

	switch (this->GetTipoPublicacion())
	{
	case eLibro: aux = "Libro";		break;
	case eRevista: aux = "Revista";	break;
	case eTesis: aux = "Tesis";		break;
	default:break;
	}

	cout << "Tipo de publicacion: " << aux << endl;
	cout << "ISBN: " << this->GetISBN() << endl;
	cout << "--------------------------------------------" << endl;
}

Libro::~Libro()
{
	
}