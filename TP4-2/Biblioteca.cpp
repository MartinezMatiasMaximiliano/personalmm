#include "Biblioteca.h"

Biblioteca::Biblioteca()
{
    this->unidadAcademica = "";
    this->directorBiblioteca = "";
}

Biblioteca::Biblioteca(string _UnidadAcademica, string _DirectorBiblioteca)
{
    this->unidadAcademica = _UnidadAcademica;
    this->directorBiblioteca = _DirectorBiblioteca;
}

string Biblioteca::GetUnidadAcademica()
{
    return this->unidadAcademica;
}

string Biblioteca::GetDirectorBiblioteca()
{
    return this->directorBiblioteca;
}

void Biblioteca::SetUnidadAcademica(string _UnidadAcademica)
{
    this->unidadAcademica = _UnidadAcademica;
}

void Biblioteca::SetDirectorBibilioteca(string _DirectorBiblioteca)
{
    this->directorBiblioteca = _DirectorBiblioteca;
}

void Biblioteca::AgregarPublicacion(Publicacion& _Publicacion)
{
    listaPublicaciones.insert(listaPublicaciones.end(), &(_Publicacion));
}

void Biblioteca::MostrarInfo()
{
    cout << "Listado dentro de la biblioteca de: " << this->GetUnidadAcademica() << endl;
    cout << "A cargo de: " << this->GetDirectorBiblioteca() << endl;

    for (int i = 0; i < listaPublicaciones.size(); i++)
    {
        listaPublicaciones[i]->MostrarInfo();
    }
}

void Biblioteca::ListarPorTipo(TipoPublicacion _TipoPublicacion)
{
    for (int i = 0; i < listaPublicaciones.size(); i++)
    {
        if (listaPublicaciones[i]->GetTipoPublicacion() == _TipoPublicacion)
        {
            listaPublicaciones[i]->MostrarInfo();
        }
    }
}

bool Biblioteca::PublicacionPertenece(string _Titulo, TipoPublicacion _TipoPublicacion)
{
    for (int i = 0; i < listaPublicaciones.size(); i++)
    {
        if (listaPublicaciones[i]->GetTitulo() == _Titulo && listaPublicaciones[i]->GetTipoPublicacion() == _TipoPublicacion)
        {
            listaPublicaciones[i]->MostrarInfo();
            return true;
        }
    }
    return false;
}

Biblioteca::~Biblioteca()
{
}
