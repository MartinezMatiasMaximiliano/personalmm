#pragma once
#include "Publicacion.h"

class Libro : public Publicacion
{
	string ISBN;
public:
	//constructores
	Libro();
	Libro(string _ISBN, string _Titulo, string _AnioEdicion, string _Autor);

	//getters
	string GetISBN();

	//setters
	void SetISBN(string _ISBN);

	//metodos
	void MostrarInfo();//definir

	//destructores
	~Libro();

private:

};

