#pragma once
#include <iostream>
#include <string>

using namespace std;

enum TipoPublicacion {eLibro, eRevista, eTesis};

class Publicacion //abstracta
{
	string titulo;
	string anioEdicion;
	string autor;
	TipoPublicacion tipoPublicacion;

public:
	//constructores
	Publicacion();
	Publicacion(string _Titulo, string _AnioEdicion, string _Autor);

	//getters
	string GetTitulo();
	string GetAnioEdicion();
	string GetAutor();
	TipoPublicacion GetTipoPublicacion();

	//setters
	void SetTitulo(string _Titulo);
	void SetAnioEdicion(string _AnioEdicion);
	void SetAutor(string _Autor);
	void SetTipoPublicacion(TipoPublicacion _TipoPublicacion);


	//metodos
	virtual void MostrarInfo() = 0; 

	//destructor
	~Publicacion();
};
