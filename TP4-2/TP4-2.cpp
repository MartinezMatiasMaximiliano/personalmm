#include <iostream>
#include "Biblioteca.h"

int main()
{
	Libro l1;
	Libro l2("32213513543", "pruebaLibro", "2017", "matias");



	Revista r1;
	Revista r2("563521651","pruebaRevista","2014","choco", Digital);
	Revista r3("56435138543", "pruebaRevista2", "2016", "luz", Impreso);




	Tesis t1;
	Tesis t2("Martinez","Lic.Inf","pruebaTesis","2018","luz");



	Biblioteca b1;
	Biblioteca b2("UNT", "Martinez");

	b2.AgregarPublicacion(l2);
	b2.AgregarPublicacion(r2);
	b2.AgregarPublicacion(t2);
	b2.AgregarPublicacion(r3);

	b2.MostrarInfo();
	cout << "===========================================================================================" << endl;
	cout << "PRUEBA DE BUSCAR REVISTAS" << endl;
	b2.ListarPorTipo(eRevista);
	
	cout << "===========================================================================================" << endl;
	cout << "PRUEBA DE BUSCAR TITULOS" << endl;
	b2.PublicacionPertenece("pruebaTesis", eTesis);

}