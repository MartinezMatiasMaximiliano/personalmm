#include "Tesis.h"

Tesis::Tesis()
{
}

Tesis::Tesis(string _Director, string _CarreraAlumno, string _Titulo, string _AnioEdicion, string _Autor) : Publicacion(_Titulo, _AnioEdicion, _Autor)
{
	this->director = _Director;
	this->carreraAlumno = _CarreraAlumno;
	this->SetTipoPublicacion(eTesis);
}

string Tesis::GetDirector()
{
	return this->director;
}

string Tesis::GetCarreraAlumno()
{
	return this->carreraAlumno;
}

void Tesis::SetDirector(string _Director)
{
	this->director = _Director;
}

void Tesis::SetCarreraAlumno(string _CarreraAlumno)
{
	this->carreraAlumno = _CarreraAlumno;
}

void Tesis::MostrarInfo()
{
	cout << "--------------------------------------------" << endl;
	cout << "[" << this->GetTitulo() << "]" << endl;
	cout << "Anio de Edicion: " << this->GetAnioEdicion() << endl;
	cout << "Autor: " << this->GetAutor() << endl;
	string aux;

	switch (this->GetTipoPublicacion())
	{
	case eLibro: aux = "Libro";		break;
	case eRevista: aux = "Revista";	break;
	case eTesis: aux = "Tesis";		break;
	default:break;
	}

	cout << "Tipo de publicacion: " << aux << endl;
	cout << "Director: " << this->GetDirector() << endl;
	cout << "Carrera del Alumno: " << this->GetCarreraAlumno() << endl;
	cout << "--------------------------------------------" << endl;
}

Tesis::~Tesis()
{
}
