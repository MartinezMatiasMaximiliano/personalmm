#pragma once
#include <iostream>
#include <vector>
#include "Publicacion.h"
#include "Libro.h"
#include "Revista.h"
#include "Tesis.h"

class Biblioteca
{
	string unidadAcademica;
	string directorBiblioteca;
	vector<Publicacion*> listaPublicaciones;

public:
	//constructores
	Biblioteca();
	Biblioteca(string _UnidadAcademica, string _DirectorBiblioteca);

	//getters
	string GetUnidadAcademica();
	string GetDirectorBiblioteca();

	//setters
	void SetUnidadAcademica(string _UnidadAcademica);
	void SetDirectorBibilioteca(string _DirectorBiblioteca);

	//metodos
	void AgregarPublicacion(Publicacion &_Publicacion);
	void MostrarInfo();
	void ListarPorTipo(TipoPublicacion _TipoPublicacion);
	bool PublicacionPertenece(string _Titulo, TipoPublicacion _TipoPublicacion);



	//destructor
	~Biblioteca();
};