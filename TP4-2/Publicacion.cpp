#include "Publicacion.h"

Publicacion::Publicacion()
{
	this->titulo = "";
	this->anioEdicion = "";
	this->autor = "";
}

Publicacion::Publicacion(string _Titulo, string _AnioEdicion, string _Autor)
{
	this->titulo = _Titulo;
	this->anioEdicion = _AnioEdicion;
	this->autor = _Autor;
}

string Publicacion::GetTitulo()
{
	return this->titulo;
}

string Publicacion::GetAnioEdicion()
{
	return this->anioEdicion;
}

string Publicacion::GetAutor()
{
	return this->autor;
}

TipoPublicacion Publicacion::GetTipoPublicacion()
{
	return this->tipoPublicacion;
}

void Publicacion::SetTitulo(string _Titulo)
{
	this->titulo = _Titulo;
}

void Publicacion::SetAnioEdicion(string _AnioEdicion)
{
	this->anioEdicion = _AnioEdicion;
}

void Publicacion::SetAutor(string _Autor)
{
	this->autor = _Autor;
}

void Publicacion::SetTipoPublicacion(TipoPublicacion _TipoPublicacion)
{
	this->tipoPublicacion = _TipoPublicacion;
}

Publicacion::~Publicacion()
{
	//no creo nada
}
