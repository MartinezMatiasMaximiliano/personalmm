#pragma once
#include <iostream>
#include "Publicacion.h"

class Tesis : public Publicacion {
	string director;
	string carreraAlumno;
public:
	//constructores
	Tesis();
	Tesis(string _Director, string _CarreraAlumno, string _Titulo, string _AnioEdicion, string _Autor);


	//getters
	string GetDirector();
	string GetCarreraAlumno();


	//setters
	void SetDirector(string _Director);
	void SetCarreraAlumno(string _CarreraAlumno);


	//metodos
	void MostrarInfo();//definir


	//destructor
	~Tesis();
};