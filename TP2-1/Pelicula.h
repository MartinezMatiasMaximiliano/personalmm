#pragma once
#include <iostream>
using namespace std;

enum  TipoPelicula { N, I };

class Pelicula
{
	int codigo;
	string titulo;
	string director;
	bool estreno;
	float precioBase;
	TipoPelicula tipoPelicula;

public:
	//constructores
	Pelicula();
	Pelicula(int _Codigo, string _Titulo, string _Director, bool _Estreno, float _PrecioBase, TipoPelicula _TipoPelicula);

	//getters
	int GetCodigo();
	string GetTitulo();
	string GetDirector();
	bool GetEstreno();
	float GetPrecio();
	TipoPelicula GetTipoPelicula();

	//metodos
	void ListarInformacion();
	float CalcularCosto();

	//destructor
	~Pelicula();
};
