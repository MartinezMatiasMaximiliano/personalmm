#include "Pelicula.h"




Pelicula::Pelicula()
{
	this->codigo = 0;
}

Pelicula::Pelicula(int _Codigo, string _Titulo, string _Director, bool _Estreno, float _PrecioBase, TipoPelicula _TipoPelicula)
{
	this->codigo = _Codigo;
	this->titulo = _Titulo;
	this->director = _Director;
	this->estreno = _Estreno;
	this->precioBase = _PrecioBase;
	this->tipoPelicula = _TipoPelicula;
}

int Pelicula::GetCodigo()
{
	return this->codigo;
}

string Pelicula::GetTitulo()
{
	return this->titulo;
}

string Pelicula::GetDirector()
{
	return this->director;
}

bool Pelicula::GetEstreno()
{
	return this->estreno;
}

float Pelicula::GetPrecio()
{
	return this->precioBase;
}

TipoPelicula Pelicula::GetTipoPelicula()
{
	return this->tipoPelicula;
}

void Pelicula::ListarInformacion()
{
	cout << "Codigo: " << this->codigo << endl;
	cout << "Titulo: " << this->titulo << endl;
	cout << "Director: " << this->director << endl;
	if (this->estreno == true)
	{
		cout << "Estreno: Si" << endl;
	}
	else {
		cout << "Estreno: No" << endl;
	}
	cout << "Precio Base: " << this->precioBase << endl;
	if (this->tipoPelicula == I)
	{
		cout << "Origen: Internacional" << endl;
	}
	else {
		cout << "Origen: Nacional" << endl;
	}

}

float Pelicula::CalcularCosto()
{
	float costo;

	costo = this->GetPrecio();

	if (this->GetTipoPelicula() == I)
	{
		costo = costo * 1.30;
	}
	else {
		if (this->GetEstreno() == false)
		{
			costo = costo * 0.8;
		}
	}
	return costo;
}

Pelicula::~Pelicula()
{
}
