#pragma once
#include <iostream>
using namespace std;

enum TipoProducto {Alimenticio, Limpieza};

class Producto
{
	int codigo;
	string descripcion;
	TipoProducto tipoProducto;
	float precioBase;
public:
	//constructores
	Producto();
	Producto(int _Codigo, string _Descripcion, TipoProducto _TipoProducto, float _PrecioBase);
	Producto(const Producto& p);


	//getters
	int GetCodigo();
	string GetDescripcion();
	TipoProducto GetTipoProducto();
	float GetPrecioBase();


	//metodos
	void MostrarProducto();
	float CalcularPrecio();



	//destructor
	~Producto();


};

