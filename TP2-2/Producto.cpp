#include "Producto.h"

float IVA = 0.105;

Producto::Producto()
{
}

Producto::Producto(int _Codigo, string _Descripcion, TipoProducto _TipoProducto, float _PrecioBase)
{
	this->codigo = _Codigo;
	this->descripcion = _Descripcion;
	this->tipoProducto = _TipoProducto;
	this->precioBase = _PrecioBase;
}

Producto::Producto(const Producto& p)
{
	this->codigo = p.codigo;
	this->descripcion = p.descripcion;
	this->tipoProducto = p.tipoProducto;
	this->precioBase = p.precioBase;
}

int Producto::GetCodigo()
{
	return this->codigo;
}

string Producto::GetDescripcion()
{
	return this->descripcion;
}

TipoProducto Producto::GetTipoProducto()
{
	return this->tipoProducto;
}

float Producto::GetPrecioBase()
{
	return this->precioBase;
}

void Producto::MostrarProducto()
{
	cout << "Codigo: " << this->codigo << endl;
	cout << "Descripcion: " << this->descripcion << endl;
	cout << "Tipo de Producto: " << this->tipoProducto << endl;
	cout << "Precio Base: " << this->precioBase << endl;
}

float Producto::CalcularPrecio()
{
    float costo = this->GetPrecioBase();

	if (this->GetTipoProducto() == Alimenticio)
	{
		costo += costo * IVA;
	}
    return costo;
}

Producto::~Producto()
{
}