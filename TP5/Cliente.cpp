#include "Cliente.h"

Cliente::Cliente()
{
	this->dni = "";
	this->nombre = "";
	this->apellido = "";
	this->direccion = "";
	this->tipoMembresia = NULL;
}

Cliente::Cliente(string _Dni, string _Nombre, string _Apellido, string _Direccion, TipoMembresia* _TipoMembresia)
{
	this->dni = _Dni;
	this->nombre = _Nombre;
	this->apellido = _Apellido;
	this->direccion = _Direccion;
	this->tipoMembresia = _TipoMembresia;
}

string Cliente::GetDni(){return this->dni;}

string Cliente::GetNombre(){return this->nombre;}

string Cliente::GetApellido(){return this->apellido;}

string Cliente::GetDireccion(){return this->direccion;}

TipoMembresia* Cliente::GetTipoMembresia()
{
	return this->tipoMembresia;
}

void Cliente::SetDni(string _Dni){this->dni = _Dni;}

void Cliente::SetNombre(string _Nombre){this->nombre = _Nombre;}

void Cliente::SetApellido(string _Apellido){this->apellido = _Apellido;}

void Cliente::SetDireccion(string _Direccion){this->direccion = _Direccion;}

void Cliente::CambiarMembresia(TipoMembresia* _TipoMembresia){this->tipoMembresia = _TipoMembresia;}

Cliente::~Cliente()
{
}


