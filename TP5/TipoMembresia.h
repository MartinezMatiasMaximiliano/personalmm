#pragma once
#include <iostream>
#include "Producto.h"
using namespace std;



class TipoMembresia {
public:
	//constructores
	TipoMembresia();

	//getters

	//setters

	//metodos
	virtual float CalcularDescuento(Producto* _Producto) = 0;
	//destructores
	~TipoMembresia();
};
