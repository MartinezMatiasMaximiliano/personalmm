#pragma once
#include <vector>
#include "Producto.h"
#include "Cliente.h"
#include "Fecha.h"
using namespace std;

class Compra {
	Fecha *fechaCompra;
	Cliente *cliente;
	vector<Producto*> listaProductos;
public:
	//constructores
	Compra();
	Compra(Fecha &_FechaCompra, Cliente &_Cliente);

	//getters
	Fecha* GetFechaCompra();
	Cliente *GetCliente();

	//setters
	void SetFechaCompra(Fecha& _FechaCompra);
	void SetCliente(Cliente& _Cliente);


	//metodos
	bool AgregarProducto(Producto& _Producto);
	float PrecioDescuento(Producto& _Producto);
	float CalcularPrecioTotal();


	//destructor
	~Compra();


};
