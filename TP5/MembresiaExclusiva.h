#pragma once
#include <iostream>
#include "TipoMembresia.h"
#include "Producto.h"

class MembresiaExclusiva : public TipoMembresia {
	const static float costoMensual;
	const static float descuentoImportados;
public: 
	//constructores
	MembresiaExclusiva();

	//getters

	//setters

	//metodos
	float CalcularDescuento(Producto *_Producto);
	//destructor
	~MembresiaExclusiva();
};