#pragma once
#include <iostream>
using namespace std;

class Producto {
	string codigo;
	string nombre;
	bool esImportado;
	float precioU;

public:
	//constructores
	Producto();
	Producto(string _Codigo, string _Nombre, bool _EsImportado, float _PrecioU);

	//getters
	string GetCodigo();
	string GetNombre();
	bool GetEsImportado();
	float GetPrecioU();

	//setters
	void SetCodigo(string _Codigo);
	void SetNombre(string _Nombre);
	void SetEsImportado(bool _EsImportado);
	void SetPrecioU(float _PrecioU);

	//metodos

	//destructor
	~Producto();

};
	ostream& operator<<(ostream& salida, Producto *p);