#pragma once
#include <iostream>
#include "TipoMembresia.h"
#include "Producto.h"

using namespace std;

class MembresiaClasica :public TipoMembresia {

public:
	MembresiaClasica();

	float CalcularDescuento(Producto* _Producto);
	~MembresiaClasica();
};
