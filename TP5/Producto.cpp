#include "Producto.h"

Producto::Producto()
{
	this->codigo = "";
	this->nombre = "";
	this->esImportado = false;
	this->precioU = 0;
}

Producto::Producto(string _Codigo, string _Nombre, bool _EsImportado, float _PrecioU)
{
	this->codigo = _Codigo;
	this->nombre = _Nombre;
	this->esImportado = _EsImportado;
	this->precioU = _PrecioU;
}

string Producto::GetCodigo(){	return this->codigo;}

string Producto::GetNombre(){	return this->nombre;}

bool Producto::GetEsImportado(){	return this->esImportado;}

float Producto::GetPrecioU(){	return this->precioU;}

void Producto::SetCodigo(string _Codigo){	this->codigo = _Codigo;}

void Producto::SetNombre(string _Nombre){	this->nombre = _Nombre;}

void Producto::SetEsImportado(bool _EsImportado){	this->esImportado = _EsImportado;}

void Producto::SetPrecioU(float _PrecioU){ this->precioU = _PrecioU; }

Producto::~Producto()
{
}

ostream& operator<<(ostream& salida, Producto* p)
{
	salida << "Codigo: " << p->GetCodigo() << endl;
	salida << "Nombre: " << p->GetNombre() << endl;
	string aux;
	if (p->GetEsImportado() == true) {aux = "Si";} else {aux = "No";}
	salida << "Importado: " << aux << endl;
	salida << "Precio: " << p->GetPrecioU() << endl;
	return salida;
}
