#include <iostream>
#include "MembresiaClasica.h"
#include "MembresiaExclusiva.h"
#include "TipoMembresia.h"
#include "Fecha.h"
#include "Compra.h"
#include "Registro.h"


int main()
{
	MembresiaClasica *Clasica = new MembresiaClasica();
	MembresiaExclusiva* Exclusiva = new MembresiaExclusiva();


	Fecha f1(12, 06, 2015);
	Producto p1("001", "Gaseosa", false, 1500);
	Producto p2("002", "Galletas", true, 2000);
	Producto p3("003", "Agua", false, 1000);
	Cliente c1("40530655", "Matias", "Martinez", "jc364", Exclusiva);

	c1.CambiarMembresia(Clasica);

	Compra prueba(f1, c1);

	prueba.AgregarProducto(p1);
	prueba.AgregarProducto(p2);
	prueba.AgregarProducto(p3);


	Registro reg;

	reg.CrearCompra(c1, f1);
	
	float test = prueba.CalcularPrecioTotal();
}

