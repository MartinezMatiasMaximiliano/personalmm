#pragma once
#include <iostream>
#include "TipoMembresia.h"
using namespace std;



class Cliente
{
	string dni;
	string nombre;
	string apellido;
	string direccion;
	TipoMembresia * tipoMembresia;


public:
	//constructores
	Cliente();
	Cliente(string _Dni, string _Nombre, string _Apellido, string _Direccion, TipoMembresia *_TipoMembresia);

	//getters
	string GetDni();
	string GetNombre();
	string GetApellido();
	string GetDireccion();
	TipoMembresia* GetTipoMembresia();
	
	//setters
	void SetDni(string _Dni);
	void SetNombre(string _Nombre);
	void SetApellido(string _Apellido);
	void SetDireccion(string _Direccion);

	//metodos
	void CambiarMembresia(TipoMembresia *_TipoMembresia);
	//destructor
	~Cliente();
	


};


