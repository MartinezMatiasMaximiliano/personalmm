#include "MembresiaExclusiva.h"

const float MembresiaExclusiva::costoMensual = 4000;
const float MembresiaExclusiva::descuentoImportados = 0.20;

MembresiaExclusiva::MembresiaExclusiva(){}

float MembresiaExclusiva::CalcularDescuento(Producto* _Producto)
{
	float total = _Producto->GetPrecioU();
	
	if (_Producto->GetPrecioU() > 1500 && _Producto->GetEsImportado() == true)
	{
		total -= _Producto->GetPrecioU() * descuentoImportados;
	}

	return total;
}

MembresiaExclusiva::~MembresiaExclusiva(){}
