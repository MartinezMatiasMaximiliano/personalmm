#pragma warning(disable : 4996)
#include "Fecha.h"
#include <ctime>

namespace std {
	bool Fecha::esDiaValido() const
	{
		if (dia >= 1 && dia <= diasEnMes(mes))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	bool Fecha::esMesValido() const
	{
		if (mes >= 1 && mes <= 12)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	bool Fecha::esAnioValido() const
	{
		if (anio > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	short Fecha::diasEnMes(const short int mes) const
	{
		
		switch (mes)
		{
		case 1:
			return 31;
			break;
		case 2:
			if ((mes%4 == 0) && (mes%100 != 0 || mes%400 == 0))
			{
				return 29;
			}
			else
			{
				return 28;
			}
			break;
		case 3:
			return 31;
			break;
		case 4:
			return 30;
			break;
		case 5:
			return 31;
			break;
		case 6:
			return 30;
			break;
		case 7:
			return 31;
			break;
		case 8:
			return 31;
			break;
		case 9:
			return 30;
			break;
		case 10:
			return 31;
			break;
		case 11:
			return 30;
			break;
		case 12:
			return 31;
			break;
		default:
			return 999999;
			break;
		}
	}
	Fecha::Fecha()
	{
		setFechaActual();
	}
	Fecha::Fecha(short int dia, short int mes, short int anio) {
		this->dia = dia;
		this->mes = mes;
		this->anio = anio;
	}
	int Fecha::RetornaDia()
	{
		return this->dia;
	}
	int Fecha::RetornaMes()
	{
		return this->mes;
	}
	int Fecha::RetornaAnio()
	{
		return this->anio;
	}
	void Fecha::setFechaActual(void) {
		struct tm* ptr;
		time_t sec;

		time(&sec);
		ptr = localtime(&sec);
		dia = (short)ptr->tm_mday;
		mes = (short)ptr->tm_mon +1;
		anio = (short)ptr->tm_year +1900;
	}
	Fecha::~Fecha(){}
	ostream& operator<<(ostream& salida, Fecha* _Fecha)
	{
		salida << _Fecha->RetornaDia() << "/" << _Fecha->RetornaMes() << "/" << _Fecha->RetornaAnio();
		return salida;
	}
}