#include "Compra.h"

Compra::Compra()
{
	this->fechaCompra = NULL;
	this->cliente = NULL;
}

Compra::Compra(Fecha& _FechaCompra, Cliente& _Cliente)
{
	this->fechaCompra = &_FechaCompra;
	this->cliente = &_Cliente;
}

Fecha *Compra::GetFechaCompra()
{
	return this->fechaCompra;
}

Cliente* Compra::GetCliente()
{
	return this->cliente;
}

void Compra::SetFechaCompra(Fecha& _FechaCompra)
{
	this->fechaCompra = &_FechaCompra;
}

void Compra::SetCliente(Cliente& _Cliente)
{
	this->cliente = &_Cliente;
}

bool Compra::AgregarProducto(Producto& _Producto)
{

	listaProductos.insert(listaProductos.end(), &_Producto);
	return true;
}

float Compra::PrecioDescuento(Producto& _Producto)
{
	return this->GetCliente()->GetTipoMembresia()->CalcularDescuento(&_Producto);
}

float Compra::CalcularPrecioTotal()
{
	float aux = 0;
	for (int i = 0; i < listaProductos.size(); i++)
	{
		aux += this->PrecioDescuento(*(listaProductos[i]));
	}
	return aux;
}

Compra::~Compra()
{
	for (int i = 0; i < listaProductos.size(); i++)
	{
		delete listaProductos[i];
	}
}
