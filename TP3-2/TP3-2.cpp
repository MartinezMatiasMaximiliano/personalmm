#include <iostream>
#include "Renglon.h"
#include "Producto.h"
#include "Factura.h"
int main()
{
	Producto P1;
	Producto P2("Q12ER","Queso cremoso", 123.456);
	Producto P3("J02ER", "Jamon crudo", 223.456);

	Factura F1("Matias Martinez");

	F1.AgregarRenglon(P2, 6);
	F1.AgregarRenglon(P3, 2);


	F1.ImprimirFactura();
}

