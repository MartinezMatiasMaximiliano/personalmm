#include "Factura.h"

int contador = 0;

Factura::Factura()
{
	this->nombreCliente = "";
}

Factura::Factura(string _NombreCliente):fechaEmision()
{
	contador++;
	this->numeroFactura = contador;
	this->nombreCliente = _NombreCliente;
}

int Factura::GetNumeroFactura()
{
	return this->numeroFactura;
}

string Factura::GetNombreCliente()
{
	return this->nombreCliente;
}

Fecha Factura::GetFecha()
{
	return this->fechaEmision;
}

bool Factura::AgregarRenglon(Producto &_In,int _Cantidad)
{
	if (this->listaRenglones.size() <10)
	{
		Renglon* nuevoRenglon = new Renglon(_In, _Cantidad);
		listaRenglones.insert(listaRenglones.end(), nuevoRenglon);
		return true;
	}
	else {
		cout << "La factura solo admite 10 renglones!" << endl;
		return false;
	}

}

float Factura::CalcularTotal()
{
	float total = 0;
	for (int i = 0; i < listaRenglones.size(); i++) {
		total += listaRenglones[i]->CalcularParcial();
	}
	return total;

}

void Factura::ImprimirFactura()
{
	cout << "Factura num. " << this->numeroFactura;
	cout << "\nFecha: " << fechaEmision.RetornaDia() << "/" << fechaEmision.RetornaMes() << "/" << fechaEmision.RetornaAnio();
	cout << "\nCliente: " << this->nombreCliente;

	cout << "\n---------------------------------------------------------------------------------------";
	cout << "\nCODIGO\tDESCRIPCION\t\t\tPRECIO U.\tCANTIDAD\tMONTO";
	cout << "\n---------------------------------------------------------------------------------------\n";

	for (int i = 0; i < listaRenglones.size(); i++)
	{
		listaRenglones[i]->MostrarRenglon();
		cout << endl;
	}

	cout << "---------------------------------------------------------------------------------------";
	cout << "\nTotal = " << this->CalcularTotal() << endl;
}

Factura::~Factura()
{
	for (int i = 0; i < listaRenglones.size(); i++)
	{
		delete listaRenglones[i];
	}
}
