#include "Producto.h"

Producto::Producto()
{
	this->codigo = "";
	this->descripcion = "";
	this->precioU = 0;
}

Producto::Producto(string _Codigo, string _Descripcion, float _PrecioU)
{
	this->codigo = _Codigo;
	this->descripcion = _Descripcion;
	this->precioU = _PrecioU;
}

Producto::Producto(const Producto& _in)
{
	this->codigo = _in.codigo;
	this->descripcion = _in.descripcion;
	this->precioU = _in.precioU;
}

string Producto::GetCodigo()
{
	return this->codigo;
}

string Producto::GetDescripcion()
{
	return this->descripcion;
}

float Producto::GetPrecio() {
	return this->precioU;
}


void Producto::MostrarProducto() {
	cout << "Codigo: " << this->codigo << endl;
	cout << "Descripcion: " << this->descripcion << endl;
	cout << "Precio unitario: " << this->precioU << endl;
}

Producto::~Producto()
{
}
