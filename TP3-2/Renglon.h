#pragma once
#include <iostream>
#include "Producto.h"
using namespace std;

class Renglon
{
	Producto* producto;
	int cantidad;
	float montoTotal;

public:
	//constructor
	Renglon();
	Renglon(Producto& _Producto, int _Cantidad);

	//getters
	Producto GetProducto();
	int GetCantidad();
	float GetMontoTotal();

	//metodos
	float CalcularParcial();
	void MostrarRenglon();

	//destructor
	~Renglon();
};

