#include "Renglon.h"

Renglon::Renglon()
{
	this->producto = NULL;
	this->cantidad = 0;
	this->montoTotal = 0;
}

Renglon::Renglon(Producto& _Producto, int _Cantidad)
{
	this->producto = &_Producto;
	this->cantidad = _Cantidad;
	this->montoTotal = this->CalcularParcial();
}

Producto Renglon::GetProducto()
{
	return *(this->producto);
}

int Renglon::GetCantidad()
{
	return this->cantidad;
}

float Renglon::GetMontoTotal()
{
	return this->montoTotal;
}


float Renglon::CalcularParcial()
{
	float total;
	total = this->producto->GetPrecio() * this->cantidad;
	return total;
}

void Renglon::MostrarRenglon()
{
	cout << "[" << this->producto->GetCodigo() << "] " << this->producto->GetDescripcion();
	//aqui se pone algo para que rellene con puntitos pa que quede parejo
	cout << "\t\t" << this->producto->GetPrecio() << "\t\t" << this->cantidad << "\t\t" << this->CalcularParcial();

}

Renglon::~Renglon()
{
}
