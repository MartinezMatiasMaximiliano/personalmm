#pragma once
#include <iostream>

using namespace std;

class Producto
{
	string codigo;
	string descripcion;
	float precioU;
public:
	//constructores
	Producto();
	Producto(string _Codigo, string _Descripcion, float _PrecioU);
	Producto(const Producto& _in);


	//getters
	string GetCodigo();
	string GetDescripcion();
	float GetPrecio();


	//metodos
	void MostrarProducto();

	//desctructor
	~Producto();
};

