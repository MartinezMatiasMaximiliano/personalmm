#pragma once
#include <iostream>
#include <vector>
#include "Fecha.h"
#include "Renglon.h"
using namespace std;

class Factura
{
	int numeroFactura;
	string nombreCliente;
	Fecha fechaEmision;
	vector<Renglon*> listaRenglones;

public:
	//constructor
	Factura();
	Factura(string _NombreCliente);

	//getters
	int GetNumeroFactura();
	string GetNombreCliente();
	Fecha GetFecha();

	//metodos
	bool AgregarRenglon(Producto& _In, int _Cantidad);
	float CalcularTotal();
	void ImprimirFactura();

	//destructor
	~Factura();



};

