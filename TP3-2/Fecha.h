#pragma once
namespace std {

	class Fecha {
		short int dia;
		short int mes;
		short int anio;
		bool esDiaValido() const;
		bool esMesValido() const;
		bool esAnioValido() const;
		short diasEnMes(const short int mes) const;
	public:
		//constructores
		Fecha();
		Fecha(short int dia, short int mes, short int anio);

		//getters
		int RetornaDia();
		int RetornaMes();
		int RetornaAnio();

		//metodos
		void setFechaActual(void);

		//destructor
		~Fecha();
	};
}
