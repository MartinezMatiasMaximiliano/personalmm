#pragma once
#include "Limpieza.h"

Limpieza::Limpieza()
{
}

Limpieza::Limpieza(string _Codigo, string _Descripcion, float _PrecioU, Fecha& _FechaElaboracion) : Producto(_Codigo, _Descripcion, _PrecioU, _FechaElaboracion)
{
    this->CostoFinal();
}

float Limpieza::CostoFinal()
{
    float aux = this->GetPrecioU() * IVAlimp;
    this->SetPrecioFinal(aux);
    return aux;

}

void Limpieza::MostrarProducto()
{
    cout << "Codigo: " << this->GetCodigo() << endl;
    cout << "Descripcion: " << this->GetDescripcion() << endl;
    cout << "Precio Unitario: " << this->GetPrecioU() << endl;
    cout << "Fecha Elaboracion: " << this->GetFechaElaboracion() << endl;
    cout << "Precio Final: " << this->GetPrecioFinal() << endl;
}


Limpieza::~Limpieza()
{
}

