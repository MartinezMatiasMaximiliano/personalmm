#pragma once
#include <iostream>
#include "Producto.h"
using namespace std;

class Limpieza : public Producto {
public:
	Limpieza();
	Limpieza(string _Codigo, string _Descripcion, float _PrecioU, Fecha& _FechaElaboracion);

	float CostoFinal();
	void MostrarProducto();

	~Limpieza();
};
