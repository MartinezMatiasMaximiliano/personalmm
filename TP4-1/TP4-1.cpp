#include <iostream>
#include "Producto.h"
#include "Alimenticio.h"
#include "Limpieza.h"

int main()
{
	Fecha fInicio(1, 9, 2017);
	Fecha f2(2, 9, 2017);
	Fecha f3(3, 9, 2017);
	Fecha f4(4, 9, 2017);

	Alimenticio defA;
	Alimenticio sobreA("001", "Gaseosa", 120, fInicio, f4);

	Limpieza defL;
	Limpieza sobreL("002", "Escoba", 120, f2);

	bool test = sobreA.EstaVencido();


	sobreA.MostrarProducto();
	sobreL.MostrarProducto();

}

