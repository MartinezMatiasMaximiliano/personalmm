#pragma once
#include <iostream>
#include "Fecha.h"
using namespace std;

static const float IVAalim = 1.105;
static const float IVAlimp = 1.21;

class Producto
{
	string codigo;
	string descripcion;
	float precioU;
	float PrecioFinal;
	Fecha *fechaElaboracion;

public:
	//constructores
	Producto();
	Producto(string _Codigo, string _Descripcion, float _PrecioU, Fecha& _FechaElaboracion);

	void SetPrecioFinal(float _PrecioFinal);

	//getters
	string GetCodigo();
	string GetDescripcion();
	float GetPrecioU();
	float GetPrecioFinal();
	Fecha* GetFechaElaboracion();

	//metodos
	virtual bool EstaVencido();
	virtual float CostoFinal();
	virtual void MostrarProducto();

	//destructor
	~Producto();
};

