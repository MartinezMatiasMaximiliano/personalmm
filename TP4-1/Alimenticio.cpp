#pragma once
#include "Alimenticio.h"

Alimenticio::Alimenticio()
{
    this->FechaVencimiento = NULL;
}

Alimenticio::Alimenticio(string _Codigo, string _Descripcion, float _PrecioU, Fecha& _FechaElaboracion, Fecha& _FechaVencimiento) : Producto(_Codigo, _Descripcion, _PrecioU, _FechaElaboracion)
{
    this->FechaVencimiento = &_FechaVencimiento;
    this->CostoFinal();
}

Fecha* Alimenticio::GetFechaVencimiento()
{
    return this->FechaVencimiento;
}

bool Alimenticio::EstaVencido()
{
    Fecha Hoy;
    if (this->FechaVencimiento->RetornaAnio() < Hoy.RetornaAnio())
    {
        if (this->FechaVencimiento->RetornaMes() < Hoy.RetornaMes())
        {
            if (this->FechaVencimiento->RetornaDia() < Hoy.RetornaDia())
            {
                return true;
            }
        }
    }
    else {
        return false;
    }
}


float Alimenticio::CostoFinal()
{
    float aux = this->GetPrecioU() * IVAalim;
    this->SetPrecioFinal(aux);
    return aux;
}

void Alimenticio::MostrarProducto()
{
    cout << "Codigo: " << this->GetCodigo() << endl;
    cout << "Descripcion: " << this->GetDescripcion() << endl;
    cout << "Precio Unitario: " << this->GetPrecioU() << endl;
    cout << "Fecha Elaboracion: " << this->GetFechaElaboracion() << endl;
    cout << "Fecha Vencimiento: " << this->GetFechaVencimiento() << endl;
    cout << "Precio Final: " << this->GetPrecioFinal() << endl;
}

Alimenticio::~Alimenticio()
{
}