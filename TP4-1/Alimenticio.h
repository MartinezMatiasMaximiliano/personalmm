#pragma once
#include <iostream>
#include "Producto.h"
using namespace std;

class Alimenticio : public Producto {
	Fecha *FechaVencimiento;
public:
	//constructores
	Alimenticio();
	Alimenticio(string _Codigo, string _Descripcion, float _PrecioU, Fecha& _FechaElaboracion, Fecha& FechaVencimiento);

	//getters
	Fecha *GetFechaVencimiento();

	//metodos
	bool EstaVencido();
	float CostoFinal();
	void MostrarProducto();

	~Alimenticio();

};