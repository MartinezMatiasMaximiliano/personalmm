#include "Producto.h"

Producto::Producto()
{
    this->codigo = "";
    this->descripcion = "";
    this->precioU = 0;
    this->fechaElaboracion = NULL;
}

Producto::Producto(string _Codigo, string _Descripcion, float _PrecioU, Fecha &_FechaElaboracion)
{
    this->codigo = _Codigo;
    this->descripcion = _Descripcion;
    this->precioU = _PrecioU;
    this->fechaElaboracion = &_FechaElaboracion;
}

void Producto::SetPrecioFinal(float _PrecioFinal)
{
    this->PrecioFinal = _PrecioFinal;
}

string Producto::GetCodigo()
{
    return this->codigo;
}

string Producto::GetDescripcion()
{
    return this->descripcion;
}

float Producto::GetPrecioU()
{
    return this->precioU;
}

float Producto::GetPrecioFinal()
{
    return this->PrecioFinal;
}

Fecha* Producto::GetFechaElaboracion()
{
    return this->fechaElaboracion;
}

bool Producto::EstaVencido()
{
    return false;
}

float Producto::CostoFinal()
{
    return 0.0f; //queda asi pq se redefine
}

void Producto::MostrarProducto()
{
    //vacio pq se redefine
}


Producto::~Producto()
{
    //vacio pq no creo objetos, fecha se crea en main
}



